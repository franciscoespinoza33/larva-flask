#! /usr/bin/env python
#! -*- coding: utf-8 -*-
# This Library combines Google Oauth and User management 

import json, datetime
import requests
from flask import Flask, request, redirect, Response
from flask.ext.login import (LoginManager, current_user, login_required,
                             login_user, logout_user, UserMixin, AnonymousUserMixin)


app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('settings.py')

# User base class
class User(UserMixin):
    """User Session management Class
    """
    def __init__(self, email, id, fname="", lname="", accesstoken="", active=True):
        self.email = email
        self.id = id
        self.active = active
        self.fname = fname
        self.lname = lname
        self.accesstoken = accesstoken

    def is_active(self):
        return self.active

    def myemail(self):
        return self.email

    def get_userid(self):
        return self.id

    def get_fname(self):
        return self.fname

    def get_lname(self):
        return self.lname

"""
USER_STORE is the store of all the users. Ideally it should be in Database
"""
USERS = {
    1: User("larvaplatform@gmail.com", 1, "Claro", "Shop", "C1ar0Shop", True),
    2: User("ediaz@sears.com.mx", 1, "Claro", "Shop", "C1ar0Shop", True),
    3: User("christian.ordaz@claroshop.com", 1, "Claro", "Shop", "C1ar0Shop", True),
    4: User("mhmalagon@sears.com.mx", 1, "Claro", "Shop", "C1ar0Shop", True),
    5: User("lfernando@skalaventures.com", 1, "Claro", "Shop", "C1ar0Shop", True)
}

"""
USER_NAMES maintains a dictionary of all the users with their email address
"""
USER_NAMES = dict((u.email, u) for u in USERS.values())


class Anonymous(AnonymousUserMixin):
    name = u"Anonymous"


now = datetime.datetime.now()
text= """
        <!DOCTYPE html>
<html>
<body>

<img src="static/images/Larva.jpg" alt="Italian Trulli">
<p><strong>Bienvenido</strong></p>
<p>Seleccione fecha de extraccion<p>

<form id="my-form" action="/action_page.php">
  Fecha:
  <input type="date" name="bday">
  <input type="submit">
</form>

<script language="JavaScript">
window.addEventListener("load", function() {
  document.getElementById('my-form').addEventListener("submit", function(e) {
    e.preventDefault();
    alert("Todo listo para fecha "+document.getElementById("my-form").bday.value);
  })
});
</script>

<h3>Linio. <a href="/getLinioCSV">Click me.</a>
<br>
Mercado Libre. <a href="/getMercadoCSV">Click me.</a>
<br>
Best Buy. <a href="/getBestCSV">Click me.</a>
<br>
Liverpool. <a href="/getLiverCSV">Click me.</a>
<br>
Wallmart. <a href="/getWallCSV">Click me.</a>
<br>
Farmacia San Pablo. <a href="/getSanPaCSV">Click me.</a> </h3>
<br>
<a href="/logout">
   <button>Salir</button>
</a>
</body>
</html>
"""

@app.route("/")
def hello():
    if current_user.is_authenticated():
        return text
        #return " User " + str(current_user.myemail()) + " is logged in "+ 

    return '''  <!DOCTYPE html>
<html>
<body>
<img src="static/images/Larva.jpg" alt="Italian Trulli">
<p><strong>Bienvenido ingrese email de validacion</strong></p>
<form id="my-form" action="/login">
  Email:
  <input type="text" name="email">
  <br>
  Contraseña:
  <input type="password" name=accesstoken>
  <br>
  <input type="submit">
</form>

</body>
</html>
'''

@app.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect("/")


@app.route("/login", methods=["GET", "POST"])
def login():
    from modules.oauth2 import GeneratePermissionUrl
    if current_user.is_authenticated():
        return text

    if request.method == "GET":
        useremail = request.args.get('email','')
        token = request.args.get('accesstoken', '')
        if token == "C1ar0Shop":
            if useremail in USER_NAMES:
                loginit = login_user(USER_NAMES[useremail], remember="yes")
                return text
    if request.method == "GET" and request.args.get('email', ''):
        url = GeneratePermissionUrl(app.config['GOOGLE_CLIENT_ID'], request.args.get('email', ''),
            redirect_uri=app.config['REDIRECT_URI'], google_account_base_url=app.config['GOOGLE_ACCOUNTS_BASE_URL'])
        return redirect(url)
    return "No Email Provided"


@app.route("/oauth2callback", methods=["GET", "POST"])
def oauth2callback():
    from grexit.modules.imap.oauth2 import AuthorizeTokens
    if request.method == "GET":
        authorizationcode = request.args.get('code', '')
        useremail = request.args.get('state', '')
        response = AuthorizeTokens(app.config['GOOGLE_CLIENT_ID'],
                                   app.config['GOOGLE_CLIENT_SECRET'],
                                   authorizationcode,
                                   redirect_uri=app.config['REDIRECT_URI'],
                                   google_account_base_url=app.config['GOOGLE_ACCOUNTS_BASE_URL'])
        accesstoken = response["access_token"]
        r = requests.get('https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + accesstoken)
        j = json.loads(r.text)
        if useremail != j["email"]:
            return "Initiated e-mail does not match with the authenticated email"
        options = {}
        options["email"] = j.get("email")
        options["firstname"] = j.get("given_name")
        options["lastname"] = j.get("family_name")
        options["accesstoken"] = accesstoken
        userid = options['userid']
        u = User(options.get("email"), userid, options.get("firstname"), options.get("lastname"), accesstoken)
        USERS[userid] = u
        loginit = login_user(u, remember="yes")
        if loginit == True:
            return "Everything happened Successfullly"
        return "Some Problem happened"
    else:
        return "Ony POST requests are allowed"                                 


@app.route("/getLinioCSV")
def getLinioCSV():
    with open("extracciones/extraccionLinio.csv") as fp:
         csv = fp.read()
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=Linio"+now.strftime("%d-%m-%Y")+".csv"})

@app.route("/getMercadoCSV")
def getMercadoCSV():
    with open("extracciones/extraccionMer.csv") as fp:
         csv = fp.read()
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=MercadoLibre"+now.strftime("%d-%m-%Y")+".csv"})

@app.route("/getBestCSV")
def getBestCSV():
    with open("extracciones/extraccionbestb.csv") as fp:
         csv = fp.read()
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=BestBuy"+now.strftime("%d-%m-%Y")+".csv"})

@app.route("/getLiverCSV")
def getLiverCSV():
    with open("extracciones/extraccionLiver.csv") as fp:
         csv = fp.read()
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=Liverpool"+now.strftime("%d-%m-%Y")+".csv"})

@app.route("/getWallCSV")
def getWallCSV():
    with open("extracciones/extraccionWall.csv") as fp:
         csv = fp.read()
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=Wallmart"+now.strftime("%d-%m-%Y")+".csv"})

@app.route("/getSanPaCSV")
def getSanPaCSV():
    with open("extracciones/extraccionSanPa.csv") as fp:
         csv = fp.read()
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=SanPablo"+now.strftime("%d-%m-%Y")+".csv"})



login_manager = LoginManager()
login_manager.init_app(app)
login_manager.anonymous_user = Anonymous


@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))

if __name__ == "__main__":
    app.run(debug=True)
